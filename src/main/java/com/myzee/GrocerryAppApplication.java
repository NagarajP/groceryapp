package com.myzee;
/*
 * Add @EnableHystrixDashboard on application
 * Add management.endpoints.web.exposure.include=hystrix.stream in application.properties
 * go to browser and request for 'http:localhost:8080/hystrix'
 * then appears a dashboard and type 'http:localhost:8080/actuator/hystrix.stream'
 * Then it appears a hystrix screen. Make a request to you application using UTL and then check back the hystrix
 * dashboard screen. Now you can see a beautiful screen here.
 */
import java.lang.annotation.Target;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
@RequestMapping("/grocery")
@EnableCircuitBreaker
@EnableHystrixDashboard
public class GrocerryAppApplication {
	
	@Autowired
	private ItemDeliverAPI itemDeliver;
	
	@Autowired
	private PaymentAPI paymentAPI;

	@Bean(name="restTemplate1")
	@LoadBalanced
	public RestTemplate getTemplate() {
		System.out.println("using restTemplate1");
		return new RestTemplate();
	}
	
	@Bean(name="restTemplate")
	@LoadBalanced
	@Primary
	public RestTemplate getTemplate1() {
		System.out.println("using restTemplate");
		return new RestTemplate();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(GrocerryAppApplication.class, args);
	}

	@RequestMapping("/app")
	public String groceryDeliver() {
		ItemsList l = itemDeliver.getItemsDelivered();
		String res = paymentAPI.doPayment(l);

		return res;
	}
}
