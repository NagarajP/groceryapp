package com.myzee;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class ItemDeliverAPI {
	
	@Autowired
	private RestTemplate restTemplate1;

	@HystrixCommand(fallbackMethod="getItemsDeliveredFallback")
	public ItemsList getItemsDelivered() {
		String url = "http://GROCERY-ITEM-SERVICE/grocery/deliver";
		ItemsList l = restTemplate1.getForObject(url, ItemsList.class);
		return l;
	}
	
	public ItemsList getItemsDeliveredFallback() {
		ItemsList l = new ItemsList();
		List<Item> items = new ArrayList<>();
		items.add(new Item("dummy", 0));
		l.setItemList(items);
		return l;
	}
}
