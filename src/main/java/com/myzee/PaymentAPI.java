package com.myzee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@Service
public class PaymentAPI {
	
	@Autowired
	private RestTemplate restTemplate1;

	@HystrixCommand(fallbackMethod="doPaymentFallback")
	public String doPayment(ItemsList l) {
		String url = "http://PAYMENT-SERVICE/grocery/payment";
		String priceValue = restTemplate1.postForObject(url, l, String.class);
		return l.getItemList().toString() + " " + priceValue;
	}
	
	public String doPaymentFallback(ItemsList l) {
		return "<html><h2> "
				+ "<font color=red>"
				+ "Payment instance is down or slow, "
				+ "calling fallback! "
				+ "</font>"
				+ "</h2></html>";
	}
}
